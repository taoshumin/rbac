/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"fmt"
	"github.com/zpatrick/rbac"
	"strings"
)

type Article struct {
	ID     string
	AuthID string
	Text   string
}

func Articles() []Article {
	return []Article{
		{
			ID:     "a1",
			AuthID: "u1",
			Text:   "1",
		},
		{
			ID:     "a2",
			AuthID: "u2",
			Text:   "2",
		},
		{
			ID:     "a3",
			AuthID: "u3",
			Text:   "3",
		},
	}
}

func main() {
	rolename := "member"

	var role rbac.Role
	switch r := strings.ToLower(rolename); r {
	case "guest":
		role = NewGuestRole()
	case "member":
		role = NewMemberRole(Articles()[0].AuthID)
	case "admin":
		role = NewAdminRole()
	default:
		panic("no role")
	}

	for _, article := range Articles() {
		canRead, err := role.Can("Read", article.ID)
		if err != nil {
			panic(err)
		}

		canEdit, err := role.Can("Edit", article.ID)
		if err != nil {
			panic(err)
		}

		canDelete, err := role.Can("Delete", article.ID)
		if err != nil {
			panic(err)
		}

		canRate, err := role.Can("Rate", article.ID)
		if err != nil {
			panic(err)
		}

		fmt.Println("Read: ", canRead)
		fmt.Println("Edit: ", canEdit)
		fmt.Println("Delete: ", canDelete)
		fmt.Println("Rate: ", canRate)
		fmt.Println("============>>>", article.ID)
	}
}

func NewAdminRole() rbac.Role {
	return rbac.Role{
		RoleID: "admin",
		Permissions: []rbac.Permission{
			rbac.NewGlobPermission("*", "*"),
		},
	}
}

func NewGuestRole() rbac.Role {
	return rbac.Role{
		RoleID: "guest",
		Permissions: []rbac.Permission{
			rbac.NewGlobPermission("Read", "*"),
			rbac.NewGlobPermission("Rate", "*"),
		},
	}
}

func NewMemberRole(userID string) rbac.Role {
	return rbac.Role{
		RoleID: fmt.Sprintf("Member(%s)", userID),
		Permissions: []rbac.Permission{
			rbac.NewGlobPermission("Create", "*"),
			rbac.NewGlobPermission("Read", "*"),
			rbac.NewGlobPermission("Rate", "*"),
			rbac.NewPermission(rbac.GlobMatch("Edit"), ifArticleAuthor(userID)),
			rbac.NewPermission(rbac.GlobMatch("Delete"), ifArticleAuthor(userID)),
		},
	}
}

func ifArticleAuthor(userid string) rbac.Matcher {
	return func(target string) (bool, error) {
		for _, article := range Articles() {
			if article.ID == target {
				return article.AuthID == userid, nil
			}
		}
		return false, nil
	}
}
