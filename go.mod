module test.io/rbac

go 1.18

require github.com/zpatrick/rbac v0.0.0-20180829190353-d2c4f050cf28

require (
	github.com/ryanuber/go-glob v1.0.0 // indirect
	github.com/stretchr/testify v1.8.0 // indirect
)
